namespace DementiaWearApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Android.Runtime;
    using Android.Views;
    using Android.Widget;
    using Android.Gms.Wearable;
    using Android.Gms.Common.Apis;

    class Communicator : Java.Lang.Object, IDataApiDataListener
    {
        private readonly IGoogleApiClient _client;
        private const string path = "/communicator";

        public Communicator(Context context)
        {
            _client = new GoogleApiClientBuilder(context)
                .AddApi(WearableClass.Api)
                .Build();
        }

        public void Resume()
        {
            if (!_client.IsConnected)
            {
                _client.Connect();
                WearableClass.DataApi.AddListener(_client, this);
            }
        }

        public void Pause()
        {
            if (_client != null && _client.IsConnected)
            {
                _client.Disconnect();
                WearableClass.DataApi.RemoveListener(_client, this);
            }
        }

        public void SendData(DataMap dataMap)
        {
            Task.Run(() =>
            {
                var request = PutDataMapRequest.Create(path);
                request.DataMap.PutAll(dataMap);
                var result = WearableClass.DataApi.PutDataItem(_client, request.AsPutDataRequest()).Await();
                var success = result.JavaCast<IDataApiDataItemResult>().Status.IsSuccess ? "Ok." : "Failed!";
                Console.WriteLine(string.Format("Communicator: Sending data map {0}... {1}", dataMap, success));
            });
        }

        public void OnDataChanged(DataEventBuffer p0)
        {
            Console.WriteLine(string.Format("Communicator: Data changed ({0} data events)", p0.Count));
            for (var i = 0; i < p0.Count; i++)
            {
                var dataEvent = p0.Get(i).JavaCast<IDataEvent>();
                if (dataEvent.Type == DataEvent.TypeChanged && dataEvent.DataItem.Uri.Path == path)
                    DataReceived(DataMapItem.FromDataItem(dataEvent.DataItem).DataMap);
            }
        }

        public event Action<DataMap> DataReceived = delegate { };
    }
}
﻿/**
 * Icon created by Johan Gunnarsson from Noun Project
 */
namespace DementiaPatientApp
{
	using System;
	using System.Collections.Generic;
	using System.IO;

	using System.Threading.Tasks;

	using Android.App;
	using Android.Content;
	using Android.Gms.Common;
	using Android.Gms.Common.Apis;
	using Android.Gms.Common.Data;
	using Android.Gms.Wearable;
	using Android.Graphics;
	using Android.Net;
	using Android.OS;
	using Android.Util;
	using Android.Views;
	using Android.Widget;

	using Java.Interop;
	using Java.Lang;
	using Microsoft.WindowsAzure.MobileServices;

	[Activity (Label = "Chrono Care - Demo", Icon = "@drawable/icon")]
	public class MainActivity : Activity, IDataApiDataListener, IMessageApiMessageListener, 
	GoogleApiClient.IConnectionCallbacks, GoogleApiClient.IOnConnectionFailedListener
	{
		private int currentTime = 23;

		public const string Tag = "PillNotification";
		public GoogleApiClient _googleApiClient;
		private double batteryLevel = 200;
		private int reminderId = 0;
		private bool createPillVisible;
		private List<Pill> todaysPills = new List<Pill> ();
		List<PillTakenLog> takenToday = new List<PillTakenLog> ();

		private LinearLayout pillContainer;
		private LinearLayout createPillContainer;
		private EditText nameEditText;
        private EditText mgEditText;
		private EditText timeEditText;
        private View createPill;

		protected readonly string logTag = "MainActivity";

		public Android.Net.Uri dataItemUri;

		public static MobileServiceClient client = new MobileServiceClient(
			"https://dimentia-care-concept.azure-mobile.net/", "GMKEVTLNlJPgIDkjIhkemsaFrjuVrb90");

		#region Lifecycle

		protected override void OnCreate (Bundle bundle)
		{
			Log.Debug (logTag, "OnCreate called, Activity is becoming Active");
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// connect to android wear
			_googleApiClient = new GoogleApiClient.Builder(this)
				.AddApi(WearableClass.Api)
				.AddConnectionCallbacks(this)
				.AddOnConnectionFailedListener(this)
				.Build();

			// initialise azure connection
			CurrentPlatform.Init();

			// the layout to add pills text to
			pillContainer = FindViewById<LinearLayout>(Resource.Id.pills_container);

			// Layout to input data in app to add a pill to azure
			LayoutInflater inflater = LayoutInflater.From (this);
			createPill = inflater.Inflate (Resource.Layout.create_pill, null, false);
			createPillContainer = FindViewById<LinearLayout>(Resource.Id.create_pill_container);


			// Get relevant data from Azure tables.
			/*try{

				Task.Run (() => takenToday = GetTodaysTakenPills ().Result).Wait ();
			}
			catch(System.Exception ex){
				var test = ex.Message;
			}*/
			Task.Run (() => todaysPills = GetPills ().Result).Wait ();
			// Workout which pills are remaining to be taken today.
			FindTodaysPills ();

			//todaysPills.Add (new Pill (){ Id = "1", Name = "Simvastatin", Milligrams = 1, Time = 8 });
		}

		protected override void OnStart()
		{
			Log.Debug (logTag, "OnStart called, App is Active");
			base.OnStart();
			if (!_googleApiClient.IsConnected)
				_googleApiClient.Connect();
		}

		protected override void OnResume()
		{
			Log.Debug (logTag, "OnResume called, app is ready to interact with the user");
			base.OnResume();
		}

		protected override void OnPause()
		{
			Log.Debug (logTag, "OnPause called, App is moving to background");
			base.OnPause();
		}

		protected override void OnStop()
		{
			Log.Debug (logTag, "OnStop called, App is in the background");
			WearableClass.DataApi.RemoveListener(_googleApiClient, this);
			WearableClass.MessageApi.RemoveListener (_googleApiClient, this);

			SendExitMessage("Patient app has exited.");

			base.OnStop();
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy ();
			Log.Debug (logTag, "OnDestroy called, App is Terminating");
		}

		#endregion

		#region Layout

		private void CreatePillUI()
		{
			if (!createPillVisible) {
				createPillContainer.AddView (createPill);
				createPillVisible = true;
			} else {
				createPillContainer.RemoveView (createPill);
				createPillVisible = false;
			}
		}

		private void UpdateBatteryTextView()
		{
			TextView batteryTextView = (TextView)FindViewById (Resource.Id.batteryTextView);
			batteryTextView.SetText (BatteryLevel (), TextView.BufferType.Normal);
		}

		private void ShowTakenPillLayout(string pillName)
		{
			LayoutInflater inflater = LayoutInflater.From(this);
			View pillStatusElem = inflater.Inflate(Resource.Layout.pill_status_element, null, false);
			((TextView)pillStatusElem.FindViewById(Resource.Id.pillTextView)).SetText(pillName, TextView.BufferType.Normal);
			pillContainer.AddView(pillStatusElem);
		}

		/**
		 * Clear notification data and show message on phone.
		 */
		private void ShowDeletedNotificationLayout()
		{
			ClearWatchNotifications ();

			// Change battery text
			UpdateBatteryTextView ();

			LayoutInflater inflater = LayoutInflater.From(this);
			View pillStatusElem = inflater.Inflate(Resource.Layout.pill_status_element, null, false);
			((TextView)pillStatusElem.FindViewById(Resource.Id.pillTextView)).SetText("Notification deleted.", TextView.BufferType.Normal);
			pillContainer.AddView(pillStatusElem);
		}

		[Export("SetPills")]
		public void SetPills(View view)
		{
			CreatePillUI ();
		}

		[Export("CreatePill")]
		public void CreatePill(View view)
		{
			nameEditText = FindViewById <EditText> (Resource.Id.name_text);
			mgEditText = FindViewById <EditText> (Resource.Id.mg_text);
			timeEditText = FindViewById <EditText> (Resource.Id.time_text);

			if (nameEditText != null && mgEditText != null && timeEditText != null) {
				var name = nameEditText.Text;
				var milli = int.Parse(mgEditText.Text);
				var time = int.Parse(timeEditText.Text);

				// Add entry to Azure database.
				Task.Run (() => AddEntryToPillDB (name, milli, time)).Wait ();
				createPillVisible = true;
				CreatePillUI ();
			}

			// Set pills remaining to be taken in todaysPills.
			FindTodaysPills();
		}

		[Export("DeleteTodaysTakenPills")]
		public void DeleteTodaysTakenPills(View view)
		{
			if (!_googleApiClient.IsConnected)
				_googleApiClient.Connect();

			// Clear pills taken today from azure and takenToday.
			Task.Run (() => RemoveTodaysTakenPills ()).Wait ();
			// As none are now taken todaysPills are reset.
			Task.Run (() => todaysPills = GetPills ().Result).Wait ();
			// This isn't necessary as there should now be no pills taken.
			FindTodaysPills ();

			// Send alert to watch.
			var dataMap = new DataMap ();
			dataMap.PutString ("delete_message", "Today's pills have been deleted.");
			SendMessageToWearable ("/deleted", dataMap.ToByteArray ());

		}

		[Export("SendReminder")]
		public void SendReminder(View view)
		{
			currentTime = 23;
			//ClearWatchNotifications ();
			SendWatchReminder ();
		}

		[Export("SendAMReminder")]
		public void SendAMReminder(View view)
		{
			currentTime = 8;
			//ClearWatchNotifications ();
			SendWatchReminder ();
		}

		[Export("SendBatteryWarning")]
		public void SendBatteryWarning(View view)
		{
			if (!_googleApiClient.IsConnected)
				_googleApiClient.Connect();
			
			SendBatteryWarningNotification ();
		}

		#endregion

		#region Azure mobile services

		/**
		 * Add an entry to the pill table in azure.
		 * 
		 * @param name The pill name
		 * @param mg The amount required to be taken by the patient
		 * @param time The time the pill should be taken
		 */
		private async void AddEntryToPillDB(string name, int mg, int time)
		{
			// Create pill item
			Pill item = new Pill { Name = name, Milligrams = mg, Time = time };

			// Try insert the pill into the azure table.
			try
			{
				await client.GetTable<Pill>().InsertAsync(item);
			}
			catch (Java.Net.MalformedURLException)
			{
				CreateAndShowDialog(new Java.Lang.Exception("There was an error creating the Mobile Service. Verify the URL"), "Error");
			}
			catch (Java.Lang.Exception e)
			{
				CreateAndShowDialog(e, "Error");
			}

			// Also add pill item to today's pills
			todaysPills.Add (item);
		}

		/**
		 * From the pill database and the taken pills log, workout which pills the patient needs to be sent.
		 */
		/*private async Task FindPillsToSend()
		{
			takenToday = await GetTodaysTakenPills ();

			// Remove pills from todays pills if its already been taken today.
			if (takenToday.Count != 0  && todaysPills.Count > 0) {
				foreach (var tt in takenToday) {
					Pill matchingPill = null;

					foreach (var tp in todaysPills) {
						if (tt.Text.Contains(tp.Name)) {
							matchingPill = tp;
						}
					}

					if (matchingPill != null) { 
						todaysPills.Remove (matchingPill);
					}
				}
			}
		}*/

		/**
		* Get a list of pills for today (from the Azure pill table).
		* 
		* @ return List of pills to be taken today
		*/
		private async Task<List<Pill>> GetPills()
		{
			return await client.GetTable<Pill> ()
				.ToListAsync ();
		}

		/**
		* Get a list of the pills taken today (from the Azure pillTakenLog).
		* 
		* @ return List of pills already taken today
		*/
		private async Task<List<PillTakenLog>> GetTodaysTakenPills()
		{
			var todaysDate = DateTime.Now.Date;

			var taken = await client.GetTable<PillTakenLog> ()
				.Where (item => item.TakenAtDate == todaysDate.ToString("yyyy-MM-dd"))
				.ToListAsync ();

			return taken;
		}

		/**
		* This method is for testing purposes
		* Remove all of the pills taken today (from the Azure pillTakenLog).
		*/
		private async Task RemoveTodaysTakenPills()
		{
			var taken = await GetTodaysTakenPills ();

			// Try delete the taken pills from the azure table.
			try
			{
				foreach (var pill in taken) {
					await client.GetTable<PillTakenLog> ().DeleteAsync (pill);
				}
			}
			catch (Java.Net.MalformedURLException)
			{
				CreateAndShowDialog(new Java.Lang.Exception("There was an error creating the Mobile Service. Verify the URL"), "Error");
			}
			catch (Java.Lang.Exception e)
			{
				CreateAndShowDialog(e, "Error");
			}

			// Clear pills taken today.
			takenToday = new List<PillTakenLog> ();
		}

		/**
		 * Add an entry to the PillTakenLog table in azure, and the takenToday log.
		 * 
		 * @param pillName The taken pill to be added to the table.
		 */
		public async Task AddTakenPill(string pillName)
		{
			var date = DateTime.Now.Date;
			var time = DateTime.Now.TimeOfDay;

			// Create the pill log item.
			PillTakenLog item = new PillTakenLog { Text = pillName, TakenAtDate = date.ToString("yyyy-MM-dd"), TakenAtTime = time.ToString() };

			// Also add the item to the takenToday list.
			takenToday.Add (item);

			// Try insert the item into the azure table.
			try
			{
				await client.GetTable<PillTakenLog>().InsertAsync(item);
			}
			catch (Java.Net.MalformedURLException) {
				CreateAndShowDialog(new Java.Lang.Exception ("There was an error creating the Mobile Service. Verify the URL"), "Error");
			} 
			catch (Java.Lang.Exception e) {
				CreateAndShowDialog(e, "Error");
			}
		}

		/**
		 * Build error reports for attempts to add an item to Azure.
		 */
		void CreateAndShowDialog(Java.Lang.Exception exception, string title)
		{
			CreateAndShowDialog(exception.Message, title);
		}

		void CreateAndShowDialog(string message, string title)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.SetMessage(message);
			builder.SetTitle(title);
			builder.Create().Show();
		}

		#endregion

		#region Notifications

		public void OnConnected(Bundle connectionHint)
		{
			WearableClass.DataApi.AddListener(_googleApiClient, this);
			WearableClass.MessageApi.AddListener (_googleApiClient, this);
		}

		public void OnConnectionSuspended(int cause)
		{
			//ignore
		}

		public void OnConnectionFailed(ConnectionResult result)
		{
			Log.Error(Tag, "Failed to connect to Google Play Services");
		}

		public void OnMessageReceived (IMessageEvent messageEvent)
		{
			/*if (messageEvent.Path.Equals (Constants.RESET_QUIZ_PATH)) {
				RunOnUiThread (new Runnable (new Action (delegate() {
					SetMood ();
				})));
			}*/
		}

		/**
		 * Listen to a response from the watch.
		 */
		public void OnDataChanged(DataEventBuffer dataEvents)
		{
			var events = FreezableUtils.FreezeIterable(dataEvents);
			dataEvents.Close();
			RunOnUiThread(() =>
				{
					foreach (var ev in events)
					{
						var e = ((Java.Lang.Object) ev).JavaCast<IDataEvent>();
						if (e.Type == DataEvent.TypeChanged)
						{
							var dataMap = DataMapItem.FromDataItem(e.DataItem).DataMap;
							var response = dataMap.GetInt("response_type", 0);
							var pillName = dataMap.GetString("taken_pill_name") + " ";
							batteryLevel = dataMap.GetDouble("watch_battery");

							if (response == 2) {
								ShowDeletedNotificationLayout ();
								SendDelayReminderNotification ();
							} else if (response == 1) {
								// Change battery text
								UpdateBatteryTextView ();
							} else {
								// add a table on the app
								if (pillName != " ") {
									ResetWatchNotification ();

									// Show the pill name in a list on the phone.
									ShowTakenPillLayout (pillName);
									// Change battery text
									UpdateBatteryTextView ();

									Task.Run (() => AddTakenPill (pillName)).Wait ();
									FindTodaysPills ();


								}
							}
						}
					}
				});
		}

		/**
		 * Send a reminder of a pill to the android wear.
		 */
		public void SendWatchReminder()
		{
			if (!_googleApiClient.IsConnected)
				_googleApiClient.Connect();
			
			//var currentTime = DateTime.Now.Hour;

			if (!todaysPills.Equals (null) && todaysPills.Count > 0) {
				List<DataMap> pillDataMaps = new List<DataMap> ();

				foreach (var pill in todaysPills) {
					// Only send reminder for pills that are due.
					if (pill.Time <= currentTime) {
						var pillDataMap = new PillDataMap (pill.Name, FindPillPicture (pill.Name));
						DataMap pdm = pillDataMap.PutToDataMap (new DataMap ());
						pillDataMaps.Add (pdm);
					}
				}
				IList<DataMap> dms = pillDataMaps;
				var pillReminder = new PillReminder(dms, reminderId);
				WearableClass.DataApi.PutDataItem (_googleApiClient, pillReminder.ToPutDataRequest ());
			} 
			else {
				/*var pillReminder = new PillReminder(null, 0, "No pills to take");
				WearableClass.DataApi.PutDataItem (_googleApiClient, pillReminder.ToPutDataRequest ());*/

				SendNoPillMessage("No pills to take.");
			}
			reminderId++;
		}

		/**
		 * Send a DataItem to the watch to build a low battery warning notification.
		 */
		public void SendBatteryWarningNotification()
		{
			/*var pillReminder = new PillReminder(null, 0, "Low battery");
			WearableClass.DataApi.PutDataItem (_googleApiClient, pillReminder.ToPutDataRequest ());*/

			var dataMap = new DataMap ();
			dataMap.PutString ("message", "Low battery");
			SendMessageToWearable ("/battery_warning", dataMap.ToByteArray ());
		}

		public void SendExitMessage(string exitMessage)
		{
			var dataMap = new DataMap ();
			dataMap.PutString ("exit_message", exitMessage);
			SendMessageToWearable ("/exited", dataMap.ToByteArray ());
		}

		public void SendNoPillMessage(string message)
		{
			var dataMap = new DataMap ();
			dataMap.PutString ("message", message);
			SendMessageToWearable ("/no_pills", dataMap.ToByteArray ());
		}

		public void SendDelayReminderNotification()
		{
			var dataMap = new DataMap ();
			dataMap.PutString ("message", "You will be reminded in an hour.");
			SendMessageToWearable ("/delay_reminder", dataMap.ToByteArray ());
		}

		public void SendMessageToWearable (string path, byte[] data)
		{
			WearableClass.NodeApi.GetConnectedNodes(_googleApiClient)
				.SetResultCallback (new SendMessageResultCallback (this, path, data));
		}

		/**
		 * Clear watch notifications by deleting all previous data items sent to the watch.
		 */
		public void ClearWatchNotifications()
		{
			if (_googleApiClient.IsConnected) {
				WearableClass.DataApi.DeleteDataItems (_googleApiClient, dataItemUri);
			} else {
				Log.Error (Tag, "Failed to reset data items because client is disconnected from" +
					"Google Play Services");
			}
		}

		public void ResetWatchNotification()
		{
			if (_googleApiClient.IsConnected) {
				WearableClass.DataApi.GetDataItems (_googleApiClient)
					.SetResultCallback (new ResetCallback (this));
			} else {
				Log.Error (Tag, "Failed to reset data items because client is disconnected from" +
					"Google Play Services");
			}
		}

		public void ResetDataItems (System.Collections.IList dataItemList)
		{
			if (_googleApiClient.IsConnected) {
				foreach (var item in dataItemList) {
					var dataItem = ((Java.Lang.Object)item).JavaCast<IDataItem> ();
					Android.Net.Uri dataItemUri = dataItem.Uri;
					WearableClass.DataApi.GetDataItem (_googleApiClient, dataItemUri);
				}
			} else {
				Log.Error (Tag, "Failed to reset data items because client is disconnected from Google Play Services");
			}
		}

		public void DeleteDataItems (List<Android.Net.Uri> dataItemUriList)
		{
			if (_googleApiClient.IsConnected) {
				foreach (Android.Net.Uri dataItemUri in dataItemUriList) {
					this.dataItemUri = dataItemUri;
					WearableClass.DataApi.DeleteDataItems (_googleApiClient, dataItemUri);

					SendWatchReminder ();
				}
			} else {
				Log.Error (Tag, "Failed to delete data items because client is disconnected from" +
					"Google Play Services");
			}
		}

		#endregion

		/**
		 * Check todaysPills for taken pills and remove if matches.
		 */
		private void FindTodaysPills()
		{
			// Remove pills from todays pills if its already been taken today.
			if (takenToday.Count != 0  && todaysPills.Count > 0) {
				foreach (var tt in takenToday) {
					Pill matchingPill = null;

					foreach (var tp in todaysPills) {
						if (tt.Text.Contains(tp.Name)) {
							matchingPill = tp;
						}
					}

					if (matchingPill != null) { 
						todaysPills.Remove (matchingPill);
					}
				}
			}
		}

		/**
		 * Convert the picture in resources to a byte[] to sent to wearable.
		 * 
		 * @ param pillPic The location of pill pic in resources.
		 * @ return Image as a byte[]
		 */
		private byte[] GetPillPicture(int pillPic)
		{
			using (var stream = new MemoryStream())
			{
				var bitmap = Android.Graphics.BitmapFactory.DecodeResource(this.Resources, pillPic);
				bitmap.Compress(Bitmap.CompressFormat.Jpeg, 10, stream);
				return stream.ToArray();
			}
		}

		/**
		 * Find the corresponding picture to the pill name.
		 * 
		 * @ param pillName The name of the pill.
		 * @ return A byte array of the picture of the pill.
		 */
		private byte[] FindPillPicture(string pillName)
		{
			if (pillName.Contains("Simvastatin"))
			{
				return GetPillPicture (Resource.Drawable.Simvastatin);
			}
			else if (pillName.Contains("Aspirin"))
			{
				return GetPillPicture (Resource.Drawable.Aspirin);
			}
			else if (pillName.Contains("Aricept"))
			{
				return GetPillPicture (Resource.Drawable.Aricept);
			}
			else if (pillName.Contains("Nifedipine"))
			{
				return GetPillPicture (Resource.Drawable.Nifedipine);
			}
			else if (pillName.Contains("Donepezil"))
			{
				return GetPillPicture (Resource.Drawable.Donepezil);
			}
			else if (pillName.Contains("Atorvastatin"))
			{
				return GetPillPicture (Resource.Drawable.Atorvastatin);
			}
			else
			{
				return GetPillPicture (Resource.Drawable.Another);
			}
		}

		/**
		 * String representation of the battery level.
		 * 
		 * @ return String representation of the battery level
		 */
		private string BatteryLevel ()
		{
			var s = "";

			if (batteryLevel == 200) {
				s = "Watch battery level unknown.";
			} else {
				s = string.Format("Watch Battery Level: {0}%", batteryLevel);
			}

			return s;
		}
	}
}
﻿using System;
using Android.Gms.Wearable;

namespace DementiaPatientApp
{
	public class PillDataMap
	{
		private string name;
		private byte[] pic;

		public PillDataMap(string pillName, byte[] pillPic)
		{
			if (pillName.Contains ("Aspirin")) {
				name = "Aspirin (x2)";
			} else {
				name = pillName;
			}
			pic = pillPic;
		}

		public PillDataMap(DataMap map)
		{
			name = map.GetString ("name");
			pic = map.GetByteArray("pic");
		}

		public DataMap PutToDataMap(DataMap map)
		{
			map.PutString ("name", name);
			map.PutByteArray ("pic", pic);
			return map;
		}
	}
}


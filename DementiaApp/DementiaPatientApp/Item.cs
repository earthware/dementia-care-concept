﻿namespace DementiaPatientApp
{
	using System;
	using Newtonsoft.Json;

	public class Item
	{
		public string Id { get; set; }

		[JsonProperty(PropertyName = "text")]
		public string Text { get; set; }
	}

	public class ItemWrapper : Java.Lang.Object
	{
		public Item Item { get; private set; }

		public ItemWrapper(Item item)
		{
			this.Item = item;
		}
	}
}


﻿namespace DementiaApp.Domain
{
	using System;

	public class MedicationConfirmation
	{
		public Guid PatientId { get; set; }

		public string MedicationTaken { get; set; }

		public TimeSpan TakenAt { get; set; }
	}
}

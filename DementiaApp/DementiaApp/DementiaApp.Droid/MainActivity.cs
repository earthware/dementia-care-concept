﻿namespace DementiaApp.Droid
{
	using System;

	using Android.App;
	using Android.Content.PM;
	using Android.Runtime;
	using Android.Views;
	using Android.Widget;
	using Android.OS;

	using Android.Util;
	using Java.Interop;
	using Android.Content;
	using TinyIoC;
	using DementiaApp.Dependencies;
	using DementiaApp.Droid.Dependencies;
	using Android.Gms.Common.Apis;
	using Android.Gms.Wearable;
	using Microsoft.WindowsAzure.MobileServices;
	using DementiaApp.Domain;

	[Activity(Label = "Chrono Care - Carer", Icon = "@drawable/Icon", Theme="@android:style/Theme.Holo.Light", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		const string Tag = "PhoneActivity";

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			TinyIoCContainer.Current.Register<ICheckTakenPillsProvider, CheckTakenPillsProvider>();
			TinyIoCContainer.Current.Resolve<ICheckTakenPillsProvider> ().InitialiseMobileServices ();

			global::Xamarin.Forms.Forms.Init(this, bundle);
			LoadApplication(new DementiaApp.App());
		}
	}
}
﻿namespace DementiaApp.ViewModels
{
	using DementiaApp.Droid;
	using DementiaApp.Dependencies;
	using DementiaApp.Domain;

	using PropertyChanged;
	using System;
	using System.Timers;
	using System.Collections.Generic;
	using System.Text;
	using System.Windows.Input;
	using System.Reflection;
	using Xamarin.Forms;
	using Microsoft.WindowsAzure.MobileServices;
	using TinyIoC;
	using System.Threading.Tasks;

	[ImplementPropertyChanged]
	public class HomeViewModel : BaseViewModel
	{
		private System.Timers.Timer timer;
		private Color defaultColor;
		private List<Domain.Pill> pills;
		private string[] takenPills;
		private ICheckTakenPillsProvider checkPillsTakenProvider = TinyIoCContainer.Current.Resolve<ICheckTakenPillsProvider>();
		public static MobileServiceClient client = new MobileServiceClient(
			"https://dimentia-care-concept.azure-mobile.net/", "GMKEVTLNlJPgIDkjIhkemsaFrjuVrb90");

		public HomeViewModel()
		{
			defaultColor = Color.Black;
			//defaultColor = Color.FromHex("bebebe");
			//defaultColor = Color.FromHex("333333");

			RefreshHomePage();
		}

		public override string Title
		{
			get { return "Mum Today"; }
		}

		public Command CallMumCommand
		{
			get { return new Command(() => { }); }
		}

		protected void RefreshHomePage()
		{
			Task.Factory.StartNew(() =>{

				Task.Run (() => pills = checkPillsTakenProvider.GetPatientPills ().Result).Wait ();
				Task.Run (() => takenPills = checkPillsTakenProvider.GetTakenPills ().Result).Wait ();

				if (takenPills != null)
				{
					foreach (var taken in takenPills)
					{
						SetTaken(taken);

						Pill matchingPill = null;
						foreach (var pill in pills) {
							if (taken.Contains (pill.Name)) {
								matchingPill = pill;
							}
						}

						if (matchingPill != null) {
							pills.Remove (matchingPill);
						}
					}
				}

				if (pills != null)
				{
					foreach (var pill in pills)
					{
						SetOverdue(pill);
					}
				}
				System.Threading.Thread.Sleep(2500);
				RefreshHomePage();
			});
		}

		public void SetTaken(string pill)
		{
			if (pill.Contains("Simvastatin")) {
				SimvastatinTextColour = defaultColor;
				SimvastatinCheckAlert = "check.png";
				SimvastatinIsVisible = true;
			} else if (pill.Contains ("Atorvastatin")) {
				AtorvastatinTextColour = defaultColor;
				AtorvastatinCheckAlert = "check.png";
				AtorvastatinIsVisible = true;
			} else if (pill.Contains ("Nifedipine")) {
				NifedipineTextColour = defaultColor;
				NifedipineCheckAlert = "check.png";
				NifedipineIsVisible = true;
			} else if (pill.Contains ("Aspirin")) {
				AspirinTextColour = defaultColor;
				AspirinCheckAlert = "check.png";
				AspirinIsVisible = true;
			} else if (pill.Contains ("Donepezil")) {
				DonepezilTextColour = defaultColor;
				DonepezilCheckAlert = "check.png";
				DonepezilIsVisible = true;
			} else { }

		}

		public void SetOverdue(Domain.Pill pill)
		{
			var red = Color.FromHex("ff0000");
			var currentHour = DateTime.Now.Hour;

			switch (pill.Name) {
			case "Simvastatin":
				if (currentHour > pill.Time) {
					SimvastatinTextColour = red;
					SimvastatinCheckAlert = "alert.png";
					SimvastatinIsVisible = true;
				} else { SimvastatinIsVisible = false; }
				break;
			case "Atorvastatin":
				if (currentHour > pill.Time) {
					AtorvastatinTextColour = red;
					AtorvastatinCheckAlert = "alert.png";
					AtorvastatinIsVisible = true;
				} else { AtorvastatinIsVisible = false; }
				break;
			case "Nifedipine":
				if (currentHour > pill.Time) {
					NifedipineTextColour = red;
					NifedipineCheckAlert = "alert.png";
					NifedipineIsVisible = true;
				} else { NifedipineIsVisible = false; }
				break;
			case "Aspirin":
				if (currentHour > pill.Time) {
					AspirinTextColour = red;
					AspirinCheckAlert = "alert.png";
					AspirinIsVisible = true;
				} else { AspirinIsVisible = false; }
				break;
			case "Donepezil":
				if (pill.Name.Contains ("Donepezil") && currentHour > pill.Time) {
					DonepezilTextColour = red;
					DonepezilCheckAlert = "alert.png";
					DonepezilIsVisible = true;
				} else { DonepezilIsVisible = false; }
				break;
			}
		}

		public string SimvastatinCheckAlert { get; set; }
		public bool SimvastatinIsVisible { get; set; }
		public Color SimvastatinTextColour { get; set; }

		public string AtorvastatinCheckAlert { get; set; }
		public bool AtorvastatinIsVisible { get; set; }
		public Color AtorvastatinTextColour { get; set; }

		public string NifedipineCheckAlert { get; set; }
		public bool NifedipineIsVisible { get; set; }
		public Color NifedipineTextColour { get; set; }

		public string AspirinCheckAlert { get; set; }
		public bool AspirinIsVisible { get; set; }
		public Color AspirinTextColour { get; set; }

		public string DonepezilCheckAlert { get; set; }
		public bool DonepezilIsVisible { get; set; }
		public Color DonepezilTextColour { get; set; }
	}
}